swtpm (0.6.0-1.0) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream version 0.6.0
    - swtpm:
      - Fix --print-capabilities for 'swtpm chardev'
      - Addressed potential symlink attack issue (CVE-2020-28407)
      - Fix potential buffer overflow related to largely unused data hashing
        function in control channel
      - Write files atomically using a temp file and then renaming
    - swtpm_setup:
      - Increase timeout from 10s to 30s for slower machines
      - Addressed potential symlink attack issue (CVE-2020-28407)
      - Remove TPM state file in case error occurred
      - Do not hardcode '/etc' but use SYSCONFDIR
      - Add missing .config path when using ${HOME}
    - swtpm-localca:
      - Allow passing pkcs11 PIN using signingkey_password
      - Allow passing environment variables needed for pkcs11 modules
      - Apply password for signing key when creating platform cert
      - Properly apply passwords for localca signing key
  * debian/clean: Improved and majorly cleaned up.
  * debian/control:
    - Added build dependency libjson-glib-dev (new upstream).
    - Removed build dependency tpm-tools (dropped upstream).
    - Removed build dependencies autoconf, automake (implied by debhelper).
    - Fixed `depends-on-misc-pre-depends` Lintian warning.
    - Corrected VCS URLs.
  * debian/not-installed added.
  * debian/swtpm-dev.install: Added missing static library.
  * debian/swtpm-tools.install:
    - Added missing tool swtpm-create-tpmca(8) and its manual page.
    - Removed manual page for obsolete script swtpm_setup.sh(8).
  * debian/patches:
    - New: Move /usr/share/swtpm executables to /usr/bin.
    - New: Move sample swtpm_setup.conf.in from etc/ to samples/.
    - New: Fix `bad-whatis-entry` Lintian warning in swtpm-create-tpmca(8).
    - New: Remove manpage and references to obsolete script swtpm_setup.sh(8).
    - New: Improve .gitignore.
    - Removed all previous patches, which have since been upstreamed.
  * Removed redundant debian/.gitignore and reverted illegal modification to
    upstream's .gitignore in the master (i.e. packaging) branch.
  * Updated Lintian overrides.

 -- Nick Chevsky <10644-nchevsky@users.noreply.salsa.debian.org>  Tue, 20 Jul 2021 21:55:00 -0500

swtpm (0.4.0~dev1-1) unstable; urgency=medium

  * New maintainer (Closes: #941199)
  * Added Standard-Version 4.5.0 to debian/control
  * Updated debhelper version to 12 in debian/control
  * Added Rules-Requires-Root to debian/control
  * Added Vcs-Browser and Vcs-Git to debian/control
  * Changed postinst interpreter to /usr/bin/sh
  * Converted debian/copyright to dep5-copyright format
  * Changed /usr/bin/swtpm_setup.sh to /usr/bin/swtpm_setup_helper
  * Added debian/watch file
  * Added lintian-override for manpage-without-executable, script-with-
    language-extension, and package-has-unnecessary-activation-of-ldconfig-
    trigger for upstream code

 -- Seunghun Han <kkamagui@gmail.com>  Tue, 21 Apr 2020 08:17:39 +0900
